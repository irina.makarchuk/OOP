package com.interlink.pizza;

import com.interlink.pizza.cheese.*;
import com.interlink.pizza.filling.*;
import com.interlink.pizza.spices.SweetBasil;
import com.interlink.pizza.vegetables.*;
import com.interlink.pizza.spices.Garlic;
import com.interlink.pizza.spices.Oregano;

interface Orderable {
    Dough dough = new Dough();
    Sauce sauce = new Sauce();

    Caprizzioza caprizzioza = new Caprizzioza();
    Margherita margherita = new Margherita();
    Marinara marinara = new Marinara();
    Napoletana napoletana = new Napoletana();

    DutchCheese dutchCheese = new DutchCheese();
    Gorgonzola gorgonzola = new Gorgonzola();
    Mozzarella mozzarella = new Mozzarella();
    Parmesan parmesan = new Parmesan();
    Ricotta ricotta = new Ricotta();

    Anchovy anchovy = new Anchovy();
    Chicken chicken = new Chicken();
    Egg egg = new Egg();
    Ham ham = new Ham();
    Mushrooms mushrooms = new Mushrooms();
    Sausage sausage = new Sausage();

    Garlic garlic = new Garlic();
    Oregano oregano = new Oregano();
    SweetBasil sweetBasil = new SweetBasil();

    Artichoke artichoke = new Artichoke();
    Corn corn = new Corn();
    Olives olives = new Olives();
    Peas peas = new Peas();
    Pepper pepper = new Pepper();
    Spinach spinach = new Spinach();
    Tomatoes tomatoes = new Tomatoes();

}
