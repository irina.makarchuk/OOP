package com.interlink.pizza.cheese;

import com.interlink.pizza.Cheese;

public class Parmesan extends Cheese {

    public String getName() {
        return "Parmesan";
    }

    private double price = 32.35;

    public String toString() {
        return String.format("%-12s%15.2f", "Parmesan", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}
