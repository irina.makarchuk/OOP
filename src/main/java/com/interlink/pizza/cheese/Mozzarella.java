package com.interlink.pizza.cheese;

import com.interlink.pizza.Cheese;

public class Mozzarella extends Cheese {

    public String getName() {
        return "Mozzarella";
    }

    private double price = 20.85;

    public String toString() {
        return String.format("%-12s%15.2f", "Mozzarella", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}
