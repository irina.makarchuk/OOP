package com.interlink.pizza;

import java.util.Scanner;

class BrandedAddedPizza implements Orderable {
    private Portion napoletanaAdder = new Portion();
    private Portion marinaraAdder = new Portion();
    private Portion margheritaAdder = new Portion();
    private Portion caprizziozaAdder = new Portion();
    private Portion dutchAdder = new Portion();
    private Portion gorgonzolaAdder = new Portion();
    private Portion mozzarellaAdder = new Portion();
    private Portion parmesanAdder = new Portion();
    private Portion ricottaAdder = new Portion();

    void orderBrandedAddedPizza() {
        Options options = new Options();
        Scanner scanner = new Scanner(System.in);
        options.selectPizza();
        while (true) {
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("nap")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", napoletana.getName(), "Its price is -", napoletana.calculate(napoletanaAdder.getPortion()), "UH.");
                options.addProductsOption();

            } else if (input.equalsIgnoreCase("mar")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", marinara.getName(), "Its price is -", marinara.calculate(marinaraAdder.getPortion()), "UH.");
                options.addProductsOption();

            } else if (input.equalsIgnoreCase("mag")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", margherita.getName(), "Its price is -", margherita.calculate(margheritaAdder.getPortion()), "UH.");
                options.addProductsOption();

            } else if (input.equalsIgnoreCase("cap")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", caprizzioza.getName(), "Its price is -", caprizzioza.calculate(caprizziozaAdder.getPortion()), "UH.");
                options.addProductsOption();

            } else if (input.equalsIgnoreCase("prod")) {
                addProducts();
                return;
            } else if (input.equalsIgnoreCase("quit")) {
                options.quit();
                return;
            } else {
                    System.out.println("The input data is incorrect. Please, try again");
                }
            }
        }

    private void addProducts() {
        Options options = new Options();
        System.out.format("%s\n", "Select one product (cheese) from the menu list and insert its full name in the format: \"dutchcheese\".\nAfter that enter the number of portions you'd like to order.\n\tIf you want to quit the program, press \"quit\".\n");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("dutchcheese")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", dutchCheese.getName(), "Its price is -", dutchCheese.calculate(dutchAdder.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("gorgonzola")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", gorgonzola.getName(), "Its price is -", marinara.calculate(gorgonzolaAdder.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("mozzarella")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", mozzarella.getName(), "Its price is -", margherita.calculate(mozzarellaAdder.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("parmesan")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", parmesan.getName(), "Its price is -", caprizzioza.calculate(parmesanAdder.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("ricotta")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", ricotta.getName(), "Its price is -", caprizzioza.calculate(ricottaAdder.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("calc")) {
                getBillForBrandedAddedPizza();
                return;
            } else if (input.equalsIgnoreCase("quit")) {
                options.quit();
                return;
            } else {
                    options.showIncorrectInput();
            }
        }
    }

    private void getBillForBrandedAddedPizza() {
        double sum = 0;
        System.out.printf("%-15s%-10s%-10s\n%s", "Name", "num", "price", "--------------------------------");
        if (napoletana.calculate(napoletanaAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", napoletana.getName(), napoletanaAdder.getNum(),napoletana.calculate(napoletanaAdder.getNum()));
            sum += napoletana.calculate(napoletanaAdder.getNum());
        }
        if (marinara.calculate(marinaraAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", marinara.getName(), marinaraAdder.getNum(), marinara.calculate(marinaraAdder.getNum()));
            sum += marinara.calculate(marinaraAdder.getNum());
        }
        if (margherita.calculate(margheritaAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", margherita.getName(), margheritaAdder.getNum(), margherita.calculate(margheritaAdder.getNum()));
            sum += margherita.calculate(margheritaAdder.getNum());
        }
        if (caprizzioza.calculate(caprizziozaAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", caprizzioza.getName(), caprizziozaAdder.getNum(), caprizzioza.calculate(caprizziozaAdder.getNum()));
            sum += caprizzioza.calculate(caprizziozaAdder.getNum());
        }
        if (dutchCheese.calculate(dutchAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", dutchCheese.getName(), dutchAdder.getNum(),dutchCheese.calculate(dutchAdder.getNum()));
            sum += dutchCheese.calculate(dutchAdder.getNum());
        }
        if (gorgonzola.calculate(gorgonzolaAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", gorgonzola.getName(), gorgonzolaAdder.getNum(), gorgonzola.calculate(gorgonzolaAdder.getNum()));
            sum += gorgonzola.calculate(gorgonzolaAdder.getNum());
        }
        if (mozzarella.calculate(mozzarellaAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", mozzarella.getName(), mozzarellaAdder.getNum(), mozzarella.calculate(mozzarellaAdder.getNum()));
            sum += mozzarella.calculate(mozzarellaAdder.getNum());
        }
        if (parmesan.calculate(parmesanAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", parmesan.getName(), parmesanAdder.getNum(), parmesan.calculate(parmesanAdder.getNum()));
            sum += parmesan.calculate(parmesanAdder.getNum());
        }
        if (ricotta.calculate(ricottaAdder.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", ricotta.getName(), ricottaAdder.getNum(), ricotta.calculate(ricottaAdder.getNum()));
            sum += ricotta.calculate(ricottaAdder.getNum());
        }
        System.out.printf("\n%s\n%s%26.2f", "--------------------------------", "TOTAL:", sum);
    }

}
