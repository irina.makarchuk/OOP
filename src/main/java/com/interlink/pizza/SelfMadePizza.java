package com.interlink.pizza;

import java.util.Scanner;

class SelfMadePizza implements Orderable {
    
    private Portion dutchCheeseProd = new Portion();
    private Portion gorgonzolaProd = new Portion();
    private Portion mozzarellaProd = new Portion();
    private Portion parmesanProd = new Portion();
    private Portion ricottaProd = new Portion();

    //Realization is valid only for the category Cheese
    void orderSelfMadePizza() {
        Options options = new Options();
        options.selectOwnPizza();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String cheese = scanner.nextLine();

            if (cheese.equalsIgnoreCase("dutchcheese")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", dutchCheese.getName(), "Its price is -", dutchCheese.calculate(dutchCheeseProd.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (cheese.equalsIgnoreCase("gorgonzola")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", gorgonzola.getName(), "Its price is -", marinara.calculate(gorgonzolaProd.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (cheese.equalsIgnoreCase("mozzarella")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", mozzarella.getName(), "Its price is -", margherita.calculate(mozzarellaProd.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (cheese.equalsIgnoreCase("parmesan")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", parmesan.getName(), "Its price is -", caprizzioza.calculate(parmesanProd.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (cheese.equalsIgnoreCase("ricotta")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", ricotta.getName(), "Its price is -", caprizzioza.calculate(ricottaProd.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (cheese.equalsIgnoreCase("calc")) {
                getBillForSelfMadePizza();
                return;
            } else if (cheese.equalsIgnoreCase("quit")) {
                options.quit();
                return;
            } else {
                    options.showIncorrectInput();
                }
            }
        }

    private void getBillForSelfMadePizza() {
        double sum = 0;
        System.out.printf("%-15s%-10s%-10s\n%s", "Name", "num", "price", "--------------------------------");
        System.out.printf("\n%-15s%-10s%-10.2f", dough.getName(), 1, dough.calculate(1));
        sum += dough.calculate(1);
        System.out.printf("\n%-15s%-10s%-10.2f", sauce.getName(), 1,sauce.calculate(1));
        sum += sauce.calculate(1);
        if (dutchCheese.calculate(dutchCheeseProd.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", dutchCheese.getName(), dutchCheeseProd.getNum(),dutchCheese.calculate(dutchCheeseProd.getNum()));
            sum += dutchCheese.calculate(dutchCheeseProd.getNum());
        }
        if (gorgonzola.calculate(gorgonzolaProd.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", gorgonzola.getName(), gorgonzolaProd.getNum(), gorgonzola.calculate(gorgonzolaProd.getNum()));
            sum += gorgonzola.calculate(gorgonzolaProd.getNum());
        }
        if (mozzarella.calculate(mozzarellaProd.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", mozzarella.getName(), mozzarellaProd.getNum(), mozzarella.calculate(mozzarellaProd.getNum()));
            sum += mozzarella.calculate(mozzarellaProd.getNum());
        }
        if (parmesan.calculate(parmesanProd.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", parmesan.getName(), parmesanProd.getNum(), parmesan.calculate(parmesanProd.getNum()));
            sum += parmesan.calculate(parmesanProd.getNum());
        }
        if (ricotta.calculate(ricottaProd.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", ricotta.getName(), ricottaProd.getNum(), ricotta.calculate(ricottaProd.getNum()));
            sum += ricotta.calculate(ricottaProd.getNum());
        }
        System.out.printf("\n%s\n%s%26.2f", "--------------------------------", "TOTAL:", sum);
    }

}
