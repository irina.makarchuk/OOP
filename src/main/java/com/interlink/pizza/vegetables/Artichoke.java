package com.interlink.pizza.vegetables;

import com.interlink.pizza.Vegetable;

public class Artichoke extends Vegetable {

    private double price = 10.50;

    public String toString() {
        return String.format("%-12s%15.2f", "Artichoke", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}