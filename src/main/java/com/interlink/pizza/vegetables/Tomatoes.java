package com.interlink.pizza.vegetables;

import com.interlink.pizza.Vegetable;

public class Tomatoes extends Vegetable {

    private double price = 7.25;

    public String toString() {
        return String.format("%-12s%15.2f", "Tomatoes", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}
