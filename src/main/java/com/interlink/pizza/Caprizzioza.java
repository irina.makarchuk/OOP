package com.interlink.pizza;

class Caprizzioza extends Pizza implements Orderable {

    public String getName() {
        return "Caprizzioza";
    }

    private double getPrice() {
        return mushrooms.calculate(1) + ham.calculate(1) + artichoke.calculate(1) + olives.calculate(1) + egg.calculate(1) + sauce.calculate(1) + dough.calculate(1);
    }

    @Override
    public String toString() {
        return String.format("-------Caprizzioza-------\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nTOTAL: %20.2f %s", dough, sauce, mushrooms, ham, olives, egg, artichoke, getPrice(), "UH");
    }

    public double calculate(int portion) {
        return portion * getPrice();
    }
}
