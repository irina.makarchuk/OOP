package com.interlink.pizza;

class Napoletana extends Pizza implements Orderable {

    public String getName() {
        return "Napoletana";
    }

    private double getPrice() {
        return dough.calculate(1) + sauce.calculate(1) + tomatoes.calculate(1) + mozzarella.calculate(1) + anchovy.calculate(1);
    }

    @Override
    public String toString() {
        return String.format("-------Napoletana-------\n%s\n%s\n%s\n%s\n%s\nTOTAL: %20.2f %s", dough, sauce, tomatoes, mozzarella, anchovy, getPrice(), "UH");
    }

    public double calculate(int portion) {
        return portion * getPrice();
    }
}
