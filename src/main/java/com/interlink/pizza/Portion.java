package com.interlink.pizza;

import java.util.Scanner;

class Portion {
    private int num;
            int getPortion() {
                Scanner scanner = new Scanner(System.in);
                String portions = scanner.nextLine();
                int portionNum = 0;
                while (true) {
                    try {
                        portionNum = Integer.parseInt(portions);
                    } catch (NumberFormatException e) {
                        System.err.println("Wrong value: " + e);
                    }
                        if ((portionNum > 0) && (portionNum <= 100)) {
                            System.out.println("Got it! Number of portions is: " + portions);
                            this.num = portionNum;
                            return portionNum;
                        } else {
                            System.out.println("The input data is incorrect. You should enter a digit from 1 to 100");
                            portions = scanner.nextLine();
                        }
                    }
                }

    int getNum() {
        return num;
    }
}
